#
# Eloy Cuadra <ecuadra@eloihr.net>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: planet-kde-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2021-04-02 00:23+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.03.80\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planet KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""
"El sitio de Planet KDE proporciona las noticias más recientes del Proyecto "
"KDE."

#: config.yaml:0
msgid "Add your own feed"
msgstr "Añada su propia fuente"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Bienvenido a Planet KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Esto es un lector de fuentes contenidos que recopila lo que los "
"colaboradores de la [comunidad KDE](https://kde.org) escriben en sus "
"respectivos blogs, en distintos idiomas."

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Bajar una publicación"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Subir una publicación"

#~ msgid "LANGUAGE_NAME"
#~ msgstr "Español"

#~ msgid "Global"
#~ msgstr "Global"

#~ msgid "Global RSS"
#~ msgstr "RSS global"

#~ msgid "Global Atom"
#~ msgstr "Atom global"
