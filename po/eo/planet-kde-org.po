# translation of planet-kde-org.pot to esperanto
# Copyright (C) 2022 Free Software Foundation, Inc.
# This file is distributed under the same license as the websites-planet-kde-org package.
# Oliver Kellogg <okellogg@users.sourceforge.net, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2023-07-15 17:53+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planedo KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr ""
"Retejo de Planedo KDE provizanta plej aktualajn novaĵojn de la projekto KDE"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Aldonu vian propran fluon"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Bonvenon al Planedo KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Ĉi tio estas flukunigilo kiu kolektas kion la kontribuantoj al la [KDE-"
"ikomunumo](https://kde.org) skribas en siaj respektivaj blogoj, en diversaj "
"lingvoj"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Iri malsupren je unu afiŝo"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Iri supren je unu afiŝo"
