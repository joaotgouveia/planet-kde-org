#
# Luiz Fernando Ranghetti <elchevive@opensuse.org>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: websites-planet-kde-org 1.0\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:42+0000\n"
"PO-Revision-Date: 2021-06-04 14:38-0300\n"
"Last-Translator: Luiz Fernando Ranghetti <elchevive@opensuse.org>\n"
"Language-Team: Brazilian Portuguese <kde-i18n-pt_BR@kde.org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 20.04.2\n"

#: config.yaml:0
msgid "Planet KDE"
msgstr "Planet KDE"

#: config.yaml:0
msgid "Planet KDE site providing newest news from the KDE Project"
msgstr "O Planet KDE fornece as notícias mais recentes do projeto KDE"

#: config.yaml:0
msgid "Add your own feed"
msgstr "Adicione seu próprio blog"

#: i18n/en.yaml:0
msgid "Welcome to Planet KDE"
msgstr "Bem-vindo ao Planet KDE"

#: i18n/en.yaml:0
msgid ""
"This is a feed aggregator that collects what the contributors to the [KDE "
"community](https://kde.org) are writing on their respective blogs, in "
"different languages"
msgstr ""
"Este é um agregador de blogs que coleta o que os contribuidores da "
"[comunidade KDE](https://kde.org) estão escrevendo em seus respectivos blog, "
"em diferentes idiomas"

#: i18n/en.yaml:0
msgid "Go down one post"
msgstr "Descer um post"

#: i18n/en.yaml:0
msgid "Go up one post"
msgstr "Subir um post"
